# -*- coding: utf-8 -*-
import scrapy


class Phil2Spider(scrapy.Spider):
    name = 'phil2'
    start_urls = ['https://ww2.fda.gov.ph/index.php/consumers-corner/registered-drugs-2?limit=100']


    def parse(self, response):
        for href in response.xpath('//table[@class="zebra"]//a/@href'):
            yield response.follow(href, self.parseDrugs)

        for page in range(100,18900,100):
            url = 'https://ww2.fda.gov.ph/index.php/consumers-corner/registered-drugs-2?limit=100&start={}'.format(page)
            yield scrapy.Request(url, self.parse)


    def parseDrugs(self, response):
        item = dict()
        item['url'] = response.url
        item['name'] = response.xpath('//h1[@class="title"]/text()').get()

        rawStringP = response.xpath('/html/body//div[contains(@class,"content")]/p').get()
        def get_value(s, name):
            i = s.find(name)
            if i == -1:
                return None
            while i < len(s):
                if s[i] == '/':
                    tag = ""
                    i += 1
                    while i < len(s) and s[i] != '>':
                        tag += s[i]
                        i += 1
                    if tag == 'strong':
                        i += 1
                        value = ""
                        while i < len(s) and s[i] != '<':
                            value += s[i]
                            i += 1
                        return value
                    return None
                i += 1
            return None     

        item['Registration No'] = get_value(rawStringP, "Registration No: ")
        item['Generic Name'] = get_value(rawStringP, "Generic Name: ")
        item['Brand Name'] = get_value(rawStringP, "Brand Name: ")
        item['Strength'] = get_value(rawStringP, "Strength: ")
        item['Form'] = get_value(rawStringP, "Form: ")
        item['Packing'] = get_value(rawStringP, "Packaging: ")
        item['Manufacturer'] = get_value(rawStringP, "Manufacturer: ")
        item['Trader'] = get_value(rawStringP, "Trader: ")
        item['Importer'] = get_value(rawStringP, "Importer: ")
        item['Distributor'] = get_value(rawStringP, "Importer: ")
        item['Country of Origin'] = get_value(rawStringP, "Country of Origin: ")
        item['Validity'] = get_value(rawStringP, "Validity: ")
        item['Product Interchangeability'] = get_value(rawStringP, "Product Interchangeability: ")
        item['GTIN'] = get_value(rawStringP, "GTIN: ")
        item['SRP'] = get_value(rawStringP, "SRP: ")
        yield item
