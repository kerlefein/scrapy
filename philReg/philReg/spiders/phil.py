# -*- coding: utf-8 -*-
import scrapy


class PhilSpider(scrapy.Spider):
    name = 'phil'
    
    def start_requests(self):
        urls = [
            'https://ww2.fda.gov.ph/index.php/consumers-corner/registered-drugs-2?limit=100',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)
    
    def parse(self, response):
        item = dict()
        for href in response.xpath('//table[@class="zebra"]//a/@href').getall():
            item['url'] = href
            yield item
