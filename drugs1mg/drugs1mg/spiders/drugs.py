import scrapy


class DrugsSpider(scrapy.Spider):
    name = 'drugs'
    start_urls = ['https://www.1mg.com/categories/winter-care-65']
    url_paginator = '?filter=true&pageNumber={}'

    def parse(self, response):
        cat = response.xpath('//div[contains(@class,"category-crumbs")]//a/span/text()').getall()
        for href in response.xpath('//div[@class="row style__add-padding___1bTbb style__grid-container___1Xvb-"]//div[@class="style__product-box___liepi"]/a/@href'):
            yield response.follow(href, self.parse_product, meta={'cat':cat})
        
        last_page = response.xpath('//ul[@class="list-pagination"]//li[not(@class)][last()]/a/text()').get()
        cat_page = response.xpath('(//div[contains(@class,"category-crumbs")]//a)[last()]/@href').get()
        for i in range(2, int(last_page) + 1):
            url = 'https://www.1mg.com{}{}'.format(cat_page, self.url_paginator.format(i))
            yield scrapy.Request(url, self.parse)

        for href in response.xpath('//li[contains(@class,"ChildrenItem")]//a/@href'):
            yield response.follow(href, self.parse)

    def parse_product(self, response):
        item = dict()
        item['url'] =  response.url
        item['cat_0'] = response.meta['cat']
        item['cat_1'] = response.xpath('//a[contains(@class,"breadcrumb")]/span/text()').getall()
        item['name'] = response.xpath('//h1[contains(@class,"product-title")]/text()').get()
        item['manufacturer'] = response.xpath('//div[contains(@class,"manufacturer")]/a/text()').get()
        item['manufacturer/marketer_adress'] = response.xpath('//div[contains(@class,"manufacturer-footer")]/text()[last()]').get()
        item['return'] = response.xpath('//ul[@class="ProductSpecification__product-specification___GXpMu"]/li/span/text()').extract_first()
        item['price'] = response.xpath('//div[contains(@class,"PriceDetails")]//text()[last()]').get()
        item['info'] = response.xpath('//div[contains(@class,"product-description")]').getall()
        item['discount'] = response.xpath('//span[@class="DiscountDetails__discount-percent___IfDdk"]/text()').extract_first()
        item['pics'] = response.xpath('//div[contains(@class,"thumbnail-container")]//img/@src').getall()
        item['bought_together'] = response.xpath('//div[@class="col-xs-12 col-md-12"]//a//span[@class="style__title___3i-N2"]/text()').get()
        item['similar_products'] = response.xpath('//div[contains(@class,"RelatedSkus__container")][.//h2[contains(text(),"Products similar to")]]//div[contains(@class,"style__name")]/text()').getall()
        item['also_bought'] = response.xpath('//div[contains(@class,"RelatedSkus__container")][.//h2[contains(text(),"Customers who")]]//div[contains(@class,"style__name")]/text()').getall()
        item['search_leading_to_page'] = response.xpath('//div[contains(@class,"OtcPage-revamp")]/a/text()').getall()
        item['combo_0'] = {response.xpath('//div[contains(@class,"combo-name-ab")]/text()').get():response.xpath('//div[contains(@class,"combo-price")]//text()[last()]').get()}
        item['combo_1'] = {response.xpath('(//div[contains(@class,"combo-name-ab")])[last()]/text()').get():response.xpath('(//div[contains(@class,"combo-price")])[last()]//text()[last()]').get()}
        item['combo_additional_discount'] = response.xpath('//div[contains(@class,"combo-message")]/text()').get()
        item['sold_out'] = response.xpath('//div[contains(@class,"out-of-stock")]//text()').get()
        item['best_seller_in'] = response.xpath('//div[contains(@class,"tag-wrap")][.//img[contains(@class,"best-seller")]]//a/text()').get()
        item['additional_offer'] = response.xpath('//div[contains(@class,"offer-detail")]/text()').get()
        item['variants'] = response.xpath('//div[contains(@class,"Variants")]//a/div/text()').getall()
        yield item

        for href in response.xpath('//div[contains(@class,"Variants")]//a/@href'):
            yield response.follow(href, self.parse_product, meta={'cat':response.meta['cat']})