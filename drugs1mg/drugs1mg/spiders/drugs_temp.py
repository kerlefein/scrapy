import scrapy

class DrugsSpider(scrapy.Spider):
    name = 'drugstemp'
    start_urls = ['https://www.1mg.com/categories/winter-care-65?filter=true&pageNumber=' + str(nom) for nom in range(1, 2)]

    def parse(self, response):
        for href in response.xpath('//div[@class="row style__add-padding___1bTbb style__grid-container___1Xvb-"]//div[@class="style__product-box___liepi"]/a/@href'):
            yield response.follow(href, self.parse_prod)

    def parse_prod(self, response):
        for prod_info in response.xpath('//div[@class="otc-container"]'):
            yield {
                'link': response.url,
                'cat': prod_info.xpath('//a[@class="button-text Breadcrumbs__breadcrumb___XuCvk"]/span/text()').extract(),
                'name': prod_info.xpath('//h1[@class="ProductTitle__product-title___3QMYH"]/text()').extract_first(),
                'manufacturer': prod_info.xpath('//div[@class="ProductTitle__manufacturer___sTfon"]/a/text()').extract_first(),
                'return': prod_info.xpath('//ul[@class="ProductSpecification__product-specification___GXpMu"]/li/span/text()').extract_first(),
                #'price': prod_info.xpath('//div[@class="PriceDetails__discount-div___nb724"]/text()').extract()[-1],
                'info': prod_info.xpath('//div[@class="ProductDescription__product-description___1PfGf"]').extract(),
                'discount': prod_info.xpath('//span[@class="DiscountDetails__discount-percent___IfDdk"]/text()').extract_first(),
                'pics': prod_info.xpath('//div[@class="row"]//img/@src').extract(),
                #'bought together': prod_info.xpath('//span[@class="style__title___3i-N2"]/text()').extract()[-1],
                'similar products': prod_info.xpath('//div[@class="style__name___3YOZc style__add-height___2DK4B style__large-font___2dBUf"]/text()').extract(),
                'searches leading to page': prod_info.xpath('//div[@class="OtcPage__footer___3MBWD"]/a/text()').extract(),
                #'combo': {prod_info.xpath('//div[@class="ComboPackItem__combo-name-ab___3YL20"]/text()').extract()[0]: prod_info.xpath('//div[@class="ComboPackItem__combo-price___1BuS-"]/text()').extract()[1], prod_info.xpath('//div[@class="ComboPackItem__combo-name-ab___3YL20"]/text()').extract()[-1]: prod_info.xpath('//div[@class="ComboPackItem__combo-price___1BuS-"]/text()').extract()[-1]},
                'sold out': prod_info.xpath('//div[@class="AvailableStatus__out-of-stock___2rv_7"]').extract_first(),
            }
            if prod_info.xpath('//span[@class="style__title___3i-N2"]/text()').extract_first() is not None:
                yield{
                    'bought together': prod_info.xpath('//span[@class="style__title___3i-N2"]/text()').extract()[-1],
                }
